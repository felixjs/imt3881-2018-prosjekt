#!/usr/bin/env python3
import image_utils

image_data = image_utils.load_image("lena.png")

image_utils.smooth_edge_preserving(image_data)

image_utils.write_image("lena-out.png", image_data)
