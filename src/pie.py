"""
Poisson Image Editing module
"""
import numpy as np


def poisson(u, iterations, boundary_setter=None, h=None, mask=None):
    """

    :param u: A min. 2-dimensional array to perform the posson iterations on.
    :param boundary_setter: A function that can be used to set the boundaries of the array
    :param iterations: The number of iterations to run on the array
    :param h: A function that takes one argument, the current array.
    """

    alpha = .25

    if h is None:
        h = lambda x: 0

    for t in range(iterations):
        result = np.empty_like(u)
        result[1:-1, 1:-1] = (u[1:-1, 1:-1] + alpha * get_laplace(u) - h(u)).clip(0, 1)
        if mask is not None:
            u[mask] = result[mask]
        else:
            np.copyto(u, result)

        if boundary_setter is not None:
            boundary_setter(u)


def poisson_with_diffparam(u, iterations, boundary_setter=None, d=1):
    """

    :param u: A min. 2-dimensional array to perform the poisson iterations on.
    :param boundary_setter: A function that can be used to set the boundaries of the array
    :param iterations: The number of iterations to run on the array
    :param d: An array with the same shape as the image that contains the diffusion parameter
    """

    alpha = .25

    for t in range(iterations):
        dDdU = (np.sum(get_2d_gradient(d) * get_2d_gradient(u), axis=0))[1:-1, 1:-1]
        u[1:-1, 1:-1] = (u[1:-1, 1:-1] + alpha * (d[1:-1, 1:-1] * get_laplace(u) + dDdU)).clip(0, 1)

        if boundary_setter is not None:
            boundary_setter(u)


def get_2d_gradient(u):
    return np.asarray(np.gradient(u, axis=(0, 1)))


def get_laplace(u):
    """

    :param u: A min. 2-dimensional array to take the laplace of
    :return:
    :rtype: laplace of the array
    """
    return (u[0:-2, 1:-1] +
            u[2:, 1:-1] +
            u[1:-1, 0:-2] +
            u[1:-1, 2:] -
            4 * u[1:-1, 1:-1])


def set_neumann_boundary(u):
    u[:, 0] = u[:, 1]  # Neumann boundary
    u[:, -1] = u[:, -2]  #
    u[0, :] = u[1, :]  #
    u[-1, :] = u[-2, :]  #