import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv
import pie


def load_image(filename: str, greyscale: bool = False) -> np.ndarray:
    """
    Loads image data from a file

    :param filename: The filename of the image file
    :param greyscale: Whether the image should be loaded in grayscale
    :return: The image data
    """
    image_data = plt.imread(filename)
    plt.ion()

    if greyscale:
        image_data = np.sum(image_data, 2) / 3.

    return image_data


def write_image(filename: str, image_data: np.ndarray, grayscale=False):
    """
    Writes image data to a file

    :param filename: The filename of the image file
    :param image_data: A min. 2-dimensional array containing the image data
    :param grayscale: Wether the image is in grayscale
    """

    if grayscale:
        cmap = plt.cm.gray
    else:
        cmap = None

    plt.imsave(fname=filename, arr=image_data, vmin=0, vmax=1, cmap=cmap)


def display_image(image_data: np.ndarray):
    """
    Displays the image using matplotlib

    :param image_data: A min. 2-dimensional array containing the image data
    """
    if len(image_data.shape) == 2:
        colormap = plt.cm.gray
    else:
        colormap = None

    plt.imshow(image_data, colormap)

    plt.axis('off')
    plt.draw()
    plt.show()


def inpaint(image_data: np.ndarray, iterations, mask: np.ndarray):
    """
    Inpaints the parts of the image where the mask has the value True.

    :param image_data:
    :param iterations:
    :param mask:
    """
    black = np.zeros(np.shape(image_data))
    image_data[mask] = black[mask]

    blur(image_data, iterations, mask)


def blur(image_data: np.ndarray, iterations=100, mask: np.ndarray = None):
    """
    Blurs the image by solving the diffusion equation.

    :param image_data: A min. 2-dimensional array to perform the posson iterations on.
    :param iterations: The number of iterations to run on the image
    :param mask: A mask with the same shape as the image data but with boolean contents,
        with True indicating that the blur should be applied to the specified area.
    """
    pie.poisson(image_data, iterations, pie.set_neumann_boundary, None, mask)


def amplify_contrast(image_data: np.ndarray, iterations=100):
    """
    Amplifies the contrast of the image

    :param image_data: A min. 2-dimensional array containing the image data
    :param iterations: The number of iterations to run on the image
    """
    u = image_data
    u_0 = image_data.copy()

    k = 1.8
    alpha = 0.25
    h = lambda u: k * alpha * pie.get_laplace(u_0)

    pie.poisson(u, iterations, pie.set_neumann_boundary, h)


def smooth_edge_preserving(image_data: np.ndarray, iterations=25):
    """
    Smoothes an image while still preserving sharp edge.

    :param image_data: A min. 2-dimensional array containing the image data
    :param iterations: The number of iterations to run on the image
    :return:
    """
    k = 400
    magnitude = np.linalg.norm(pie.get_2d_gradient(image_data), axis=0)
    d = 1/(1 + k * np.power(magnitude, 2))

    pie.poisson_with_diffparam(image_data, iterations, pie.set_neumann_boundary, d)


def get_faces_mask(filename: str) -> np.ndarray:
    """
    Finds faces in the image using OpenCV face detection, and returns a mask indicating where the faces are.

    :param filename: The filename of the image to find the faces in
    :return: A np.ndarray containg booleans where True indicates that there is a face
    """
    face_cascade = cv.CascadeClassifier(cv.data.haarcascades + 'haarcascade_frontalface_default.xml')

    img = cv.imread(filename)

    gray_image = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(
        gray_image,
        scaleFactor=1.1,
        minNeighbors=5
    )

    mask = np.zeros(np.shape(gray_image), dtype=bool)

    for (x, y, w, h) in faces:
        mask[y:y + h, x:x + w] = True

    return mask
