#!/usr/bin/env python3
import image_utils

image_path = "regjeringen.png"

img = image_utils.load_image(image_path)

mask = image_utils.get_faces_mask(image_path)

image_utils.blur(img, 100, mask)

image_utils.write_image("regjeringen-out.png", img)
