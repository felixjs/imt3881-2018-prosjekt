#!/usr/bin/env python3
import math
import image_utils
import time
import cv2 as cv

image_data = image_utils.load_image("musk.png")
image_data2 = image_data.copy()

start_time = time.time()

image_utils.blur(image_data)

print("Poisson blur took " + str(math.floor((time.time() - start_time) * 1000)) + " ms")

start_time = time.time()

image_data2 = cv.GaussianBlur(image_data2, (51, 51), 0)

print("OpenCV Gaussian blur took " + str(math.floor((time.time() - start_time) * 1000)) + " ms")

image_utils.write_image("musk-pieblur.png", image_data)
image_utils.write_image("musk-cvblur.png", image_data2)
