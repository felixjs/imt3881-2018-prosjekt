#!/usr/bin/env python3
import image_utils

image_data = image_utils.load_image("musk.png")

image_utils.amplify_contrast(image_data)

image_utils.display_image(image_data)
