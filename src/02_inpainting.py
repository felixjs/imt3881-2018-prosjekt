#!/usr/bin/env python3
import numpy as np
import image_utils

im = image_utils.load_image("lena.png")

mask = np.zeros(np.shape(im), dtype=bool)
mask[100:140, 150:190] = True
mask[480:512, 370:512] = True

image_utils.inpaint(im, 1000, mask)

image_utils.display_image(im)
